(function () {
    var Row, RowChanger;

    this.getModel = getModel;
    animate_status = false;

    function check_animate_status() {
        if (animate_status) {
            return true;
        } else {
            animate_status = true;
            setTimeout(function () {
                    animate_status = false;
                },
                500);
            return false;
        }
    }

    Row = {
        getCount: function () {
            return this.items.length;
        },

        /**
         * Get item, relative to the current item.
         */
        getItem: function (pos) {
            var items = this.data.items,
                cnt = items.length;
            return items[(this.data.current + (pos || 0) + cnt) % cnt];
        },

        /**
         * Move current item right.
         */
        moveNext: function () {
            this.data.current = (this.data.current + 1) % this.data.items.length;
            this.trigger('next');
        },

        /**
         * Move current item left.
         */
        movePrev: function () {
            this.data.current = (this.data.current - 1 + this.data.items.length) % this.data.items.length;
            this.trigger('prev');
        }
    };

    RowChanger = {
        getCount: function () {
            return this.data.items.length;
        },

        /**
         * Get item, relative to the current item.
         */
        getRow: function (pos) {
            var items = this.data.items,
                cnt = items.length;
            return items[(this.data.current + (pos || 0) + cnt) % cnt];
        },

        /**
         * Move current item right.
         */
        moveDown: function () {
            if (check_animate_status()) {
                return false;
            }
            this.data.current = (this.data.current + 1) % this.data.items.length;
            current_row = this.data.current;
            this.trigger('down');
        },

        /**
         * Move current item left.
         */
        moveUp: function () {
            if (check_animate_status()) {
                return false;
            }
            this.data.current = (this.data.current - 1 + this.data.items.length) % this.data.items.length;
            current_row = this.data.current;
            this.trigger('up');
        },

        /**
         * Move current item right.
         */
        moveNext: function () {
            if (check_animate_status()) {
                return false;
            }
            var k, n;
            for (k = 0, n = this.getCount(); k < n; ++k) {
                this.getRow(k).moveNext();
            }
            this.trigger('next');
        },

        /**
         * Move current item left.
         */
        movePrev: function () {
            if (check_animate_status()) {
                return false;
            }
            var k, n;
            for (k = 0, n = this.getCount(); k < n; ++k) {
                this.getRow(k).movePrev();
            }
            this.trigger('prev');
        }
    };

    function getModel() {
        var row1, row2, row3;

        row1 = _.extend({
            data: {
                current: 0,
                name: 'Art-house',
                items: [
                    {
                        name: 'Amazing stories',
                        poster: './images/posters/1.jpg',
                        price: 3.24,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure, nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Green elephant',
                        poster: './images/posters/2.jpg',
                        price: 7.98,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure, nulla commodo ut labore in in et aute et veniam ex irure, nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Drakula',
                        poster: './images/posters/3.jpg',
                        price: 1.79,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Terminator',
                        poster: './images/posters/4.jpg',
                        price: 8.09,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Terminator 2',
                        poster: './images/posters/5.jpg',
                        price: 2.22,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    }
                ]
            }
        }, Row, Events);

        row2 = _.extend({
            data: {
                current: 0,
                name: 'Detective',
                items: [
                    {
                        name: 'Woodstock',
                        poster: './images/posters/6.jpg',
                        price: 3.24,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Rock-n-rolla',
                        poster: './images/posters/7.jpg',
                        price: 7.98,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Resident Evil',
                        poster: './images/posters/8.jpg',
                        price: 1.79,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: '8 Mile',
                        poster: './images/posters/9.jpg',
                        price: 8.09,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Ice Age',
                        poster: './images/posters/10.jpg',
                        price: 2.22,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    }
                ]
            }
        }, Row, Events);

        row3 = _.extend({
            data: {
                current: 0,
                name: 'Detective',
                items: [
                    {
                        name: 'Transformers',
                        poster: './images/posters/11.jpg',
                        price: 3.24,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Kill Bill',
                        poster: './images/posters/12.jpg',
                        price: 7.98,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Dreamers',
                        poster: './images/posters/13.jpg',
                        price: 1.79,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Resident Evil 2',
                        poster: './images/posters/14.jpg',
                        price: 8.09,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    },
                    {
                        name: 'Shrek',
                        poster: './images/posters/15.jpg',
                        price: 2.22,
                        channel: "Recritube",
                        time: "07:40 - 07:39p",
                        description: "nulla commodo ut labore in in et aute et veniam ex irure"
                    }
                ]
            }
        }, Row, Events);

        return _.extend({
            data: {
                current: 0,
                items: [
                    row1,
                    row2,
                    row3
                ]
            }
        }, RowChanger, Events);
    }
}).call(this);

(function () {
    var ITEM_WIDTH = 20,
        ITEM_HEIGHT = 15,
        WTF_YSTEP = ITEM_HEIGHT * 1.2,
        WTF_ZSTEP = ITEM_HEIGHT * 0.5,
        GAP = 2;

    var scene, camera, light, renderer, model, widgets;

    stats();

    window.addEventListener('load', function main() {
        model = getModel();

        createStage();

        model.on('prev', startAnimationPrev);
        model.on('next', startAnimationNext);
        model.on('up', startAnimationUp);
        model.on('down', startAnimationDown);

        document.addEventListener('keydown', function (e) {
            switch (e.keyCode) {
                case key.LEFT:
                    model.movePrev();
                    break;
                case key.RIGHT:
                    model.moveNext();
                    break;
                case key.UP:
                    model.moveUp();
                    break;
                case key.DOWN:
                    model.moveDown();
                    break;
            }
        });
    });

    return this.createStage = createStage;

    /**
     * Create and setup scene with all object, camera and renderer.
     */
    function createStage() {
        camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        camera.position.set(0, 40, 125);
        // camera.position.set(0, 25, 75);
        camera.lookAt(new THREE.Vector3(0, 25, 0));
        camera.setLens(50, 30);

        renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });
//        console.log(renderer.info);
        renderer.setSize(window.innerWidth, window.innerHeight);

        document.body.appendChild(renderer.domElement);

        scene = new THREE.Scene();

        // createOrtho();
        createWidgets();

        light = new THREE.PointLight(0xffffff, 1.5, 150);
        light.position.set(0, 0, 15);
        scene.add(light);

        scene.add(new THREE.AmbientLight(0x909090));

        render();
    }

    function render() {
        if (TWEEN.update())
            requestAnimationFrame(render);

        renderer.render(scene, camera);
    }

    function createOrtho() {
        var material, geometry, line;

        // RED - X
        material = new THREE.LineBasicMaterial({
            color: 0xff0000,
            lineWidth: 5
        });

        geometry = new THREE.Geometry();
        geometry.vertices.push(new THREE.Vector3(0, 0, 0));
        geometry.vertices.push(new THREE.Vector3(10, 0, 0));

        scene.add(line = new THREE.Line(geometry, material));

        // GREEN - Y
        material = new THREE.LineBasicMaterial({
            color: 0x00ff00,
            lineWidth: 5
        });

        geometry = new THREE.Geometry();
        geometry.vertices.push(new THREE.Vector3(0, 0, 0));
        geometry.vertices.push(new THREE.Vector3(0, 10, 0));

        scene.add(line = new THREE.Line(geometry, material));

        // BLUE - Z
        material = new THREE.LineBasicMaterial({
            color: 0x0000ff,
            lineWidth: 5
        });

        geometry = new THREE.Geometry();
        geometry.vertices.push(new THREE.Vector3(0, 0, 0));
        geometry.vertices.push(new THREE.Vector3(0, 0, 10));

        scene.add(line = new THREE.Line(geometry, material));
    }

    function stats() {
        var stats = new Stats();
        stats.setMode(0); // 0: fps, 1: ms

        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = '0px';
        stats.domElement.style.top = '0px';

        document.body.appendChild( stats.domElement );

        setInterval( function () {
            stats.begin();

            // your code goes here

            stats.end();
        }, 1000 / 60 );
    }

    function createWidgets() {
        var widget, n, m;

        widgets = [];

        for (n = 0; n <= 3; ++n) {
            for (m = -4; m <= 4; ++m) {
                createWidget(n, m, makeWidgetParam(n, m));
            }
        }
    }

    function createWidget(row, col, param) {
        var widget, item, geometry, material, mesh, tween;

        item = model.getRow(row).getItem(col);

        function updateParam() {
            _.extend(param, this);
            applyParam();
        };

        function applyParam() {
            mesh.position.set(param.x, param.y, param.z + Math.pow(Math.abs(param.x)/14, 2));
//            mesh.rotation.set(- param.y/10 * Math.PI / 180, - param.x/2 * Math.PI / 180, 0);
            mesh.rotation.set(- param.y/5 * Math.PI / 180, - param.x/1.8 * Math.PI / 180, 0);

            if (param.x == 0 && param.y == 18) {
                reshape({x: 0, y: mesh.position.y + 9, z: 50});
            }
        };

        function reshape(toParam, remove) {
            if (tween)
                tween.onComplete(null).stop();

            tween = new TWEEN.Tween(param).
                to(toParam, 500).
                onUpdate(updateParam).
                onComplete(remove ? removeWidget : null).
                easing(TWEEN.Easing.Quadratic.InOut).
                start();
        }

        function removeWidget() {
            var i = _.indexOf(widgets, widget);
            if (i != -1)
                widgets.splice(i, 1);
            scene.remove(mesh);
        }

        material = new THREE.MeshLambertMaterial({
            map: item.posterTexture || THREE.ImageUtils.loadTexture(item.poster, null, function (texture) {
                item.posterTexture = texture;
                render();
            })
        });

//        var map = item.posterTexture || THREE.ImageUtils.loadTexture(item.poster, null, function (texture) {
//            item.posterTexture = texture;
//            render();
//        });
//
//        var cubeFaces = new Array();
//        cubeFaces.push(new THREE.MeshBasicMaterial({color: new THREE.Color(0x000000)}));   // 1
//        cubeFaces.push(new THREE.MeshBasicMaterial({color: new THREE.Color(0x000000)}));   // 3
//        cubeFaces.push(new THREE.MeshBasicMaterial({color: new THREE.Color(0x000000)}));   // 4
//        cubeFaces.push(new THREE.MeshBasicMaterial({color: new THREE.Color(0x000000)}));   // 5
//        cubeFaces.push(new THREE.MeshLambertMaterial({map: map}));                         // 0
//        cubeFaces.push(new THREE.MeshBasicMaterial({color: new THREE.Color(0x000000)}));   // 2
//
//        material = new THREE.MeshFaceMaterial(cubeFaces);

        geometry = new THREE.PlaneGeometry(param.width, param.height);
//        geometry = new THREE.CubeGeometry(param.width, param.height, 3);

        mesh = new THREE.Mesh(geometry, material);

        applyParam();

        scene.add(mesh);

        return widgets.push(widget = {
            param: param,
            row: row,
            col: col,
            reshape: reshape
        }), widget;
    }

    function getWidget(row, col) {
        return _.find(widgets, function (widget) {
            return widget.row === row && widget.col === col;
        });
    }

    function makeWidgetParam(row, col) {
        var position = {
            x: 0,
            y: 0,
            z: 0
        };

        if (row < 0)
            row = -5;

        if (row > 3)
            row = 10;

        if (col < -4)
            col = -15;

        if (col > 4)
            col = 15;

        position.z = -row * WTF_ZSTEP;
        position.y = row * WTF_YSTEP;
        position.x = col * (ITEM_WIDTH + GAP);

        return _.extend(position, {
            width: ITEM_WIDTH,
            height: ITEM_HEIGHT,
//            rotation: - col * 20 * Math.PI / 180
        });
    }

    function notShown(widget) {
        return widget.row < 0 || widget.row > 3
            || widget.col < -4 || widget.col > 4;
    }

    function startAnimationPrev() {
        var w, k, kk;

        for (k = 0, kk = widgets.length; k < kk; ++k) {
            (w = widgets[k]).reshape(makeWidgetParam(w.row, --w.col), notShown(w));
        }

        for (k = 0; k <= 3; ++k) {
            if (getWidget(k, 4) == null) {
                createWidget(k, 4, makeWidgetParam(k, 5)).
                    reshape(makeWidgetParam(k, 4));
            }
        }

        render();
    }

    function startAnimationNext() {
        var w, k, kk;

        for (k = 0, kk = widgets.length; k < kk; ++k) {
            (w = widgets[k]).reshape(makeWidgetParam(w.row, ++w.col), notShown(w));
        }

        for (k = 0; k <= 3; ++k) {
            if (getWidget(k, -4) == null) {
                createWidget(k, -4, makeWidgetParam(k, -5)).
                    reshape(makeWidgetParam(k, -4));
            }
        }

        render();
    }

    function startAnimationUp() {
        var w, k, kk;

        for (k = 0, kk = widgets.length; k < kk; ++k) {
            (w = widgets[k]).reshape(makeWidgetParam(++w.row, w.col), notShown(w));
        }

        for (k = -4; k <= 4; ++k) {
            if (getWidget(0, k) == null) {
                createWidget(0, k, makeWidgetParam(-1, k)).
                    reshape(makeWidgetParam(0, k));
            }
        }

        render();
    }

    function startAnimationDown() {
        var w, k, kk;

        for (k = 0, kk = widgets.length; k < kk; ++k) {
            (w = widgets[k]).reshape(makeWidgetParam(--w.row, w.col), notShown(w));
        }

        for (k = -4; k <= 4; ++k) {
            if (getWidget(3, k) == null) {
                createWidget(3, k, makeWidgetParam(4, k)).
                    reshape(makeWidgetParam(3, k));
            }
        }

        render();
    }

}).call(this);