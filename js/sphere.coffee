class Monitor
  constructor: ->
    @width = window.innerWidth
    @height = window.innerHeight

    @scene = null
    @camera = null
    @pointLight = null
    @renderer = null

    @container = $('#container')

    @gui = new dat.GUI()

  init: ->
    @initScene()
    @initCamera()
    @initRenderer()
    @initPointLight()
    @stats()

  initScene: ->
    @scene = new THREE.Scene()

  initCamera: ->
    camera_angle = 45
    screen_aspect = @width / @height
    near = 0.1
    far = 10000
    camera_position_z = 1000

    @camera = new THREE.PerspectiveCamera(camera_angle, screen_aspect, near, far)
    @camera.position.set(0, 0, camera_position_z)

  initRenderer: ->
    @renderer = new THREE.WebGLRenderer(
      antialias: true
      alpha: true
    )
    @renderer.setSize(@width, @height)

    console.log(@renderer.info);

    @container.append(@renderer.domElement)

  initPointLight: ->
    @pointLight = new THREE.PointLight(0xFFFFFF)
    @pointLight.position.set(250, 50, 1000)

    @scene.add(@pointLight)
    @scene.add(new THREE.AmbientLight(0x909090))

  render: =>
    requestAnimationFrame(@render)
    @renderer.render(@scene, @camera)

  stats: ->
    stats = new Stats()
    stats.setMode 0 # 0: fps, 1: ms
    stats.domElement.style.position = "absolute"
    stats.domElement.style.left = "5px"
    stats.domElement.style.top = "5px"
    document.body.appendChild stats.domElement
    setInterval (->
      stats.begin()
      # your code goes here
      stats.end()
      return
    ), 1000 / 60
    return




class Sphere
  constructor: (monitor) ->
    @monitor = monitor
    @sphere = null
    @smallSphere = null

    @model = window.model

  createPlane: ->
    canvas = document.createElement('canvas')
    canvas.width = 100
    canvas.height = 100

    context = canvas.getContext('2d')
    context.fillStyle = "#136579";
    context.fillRect( 0, 0, 100, 100 );

    texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;

    return texture



  testSphere: () ->
#    width =
#    height =

    geometry = new THREE.SphereGeometry( 300, 40, 40, Math.PI / 2, Math.PI * 2, 0, Math.PI )
#    material = new THREE.MeshLambertMaterial(color: new THREE.Color( 0x999999 ))
    material = new THREE.MeshLambertMaterial({map: @createPlane()})
#    material.side = THREE.BackSide

    @sphere = new THREE.Mesh(geometry, material)
    @sphere.overdraw = true;

    @monitor.scene.add(@sphere)

  testSmallSphere: () ->
    geometry1 = new THREE.PlaneGeometry(50, 30);
    material = new THREE.MeshLambertMaterial({wireframe: true})


    geometry = new THREE.SphereGeometry( 100, 20, 20, Math.PI / 2, Math.PI * 2, 0, Math.PI )
    material = new THREE.MeshLambertMaterial({wireframe: true})

    @smallSphere = new THREE.Mesh(geometry, material)
    @smallSphere.overdraw = true;

    @smallSphere.position.x = 450

    @monitor.scene.add(@smallSphere)

  animation: =>
    requestAnimationFrame(@animation)
#    console.log(@monitor.camera.rotation)
#    @monitor.camera.rotation.x += 0.001
#    @monitor.camera.rotation.y += 0.001
    @monitor.camera.rotation.z += 0.01
    @smallSphere.rotation.z += 0.01
#    @sphere.scale.x -= 0.01
#    @sphere.scale.y -= 0.01
#    @sphere.scale.z += 0.01
    @monitor.renderer.render(@monitor.scene, @monitor.camera)





@model = window.model

window.Monitor = Monitor
window.Sphere = Sphere

monitor = new Monitor
sphere = new Sphere(monitor)

monitor.init()
monitor.render()
console.log(monitor.camera.rotation)
monitor.gui.add(monitor.camera.position, 'x').min(-1000).max(1000).step(10);
monitor.gui.add(monitor.camera.position, 'y').min(-1000).max(1000).step(10);
monitor.gui.add(monitor.camera.position, 'z').min(-1000).max(1000).step(10);
monitor.gui.add(monitor.camera.rotation, 'x').min(-100).max(100).step(0.1);
monitor.gui.add(monitor.camera.rotation, 'y').min(-100).max(100).step(0.1);
monitor.gui.add(monitor.camera.rotation, 'z').min(-100).max(100).step(0.1);

sphere.testSphere()
sphere.testSmallSphere()
sphere.animation()
